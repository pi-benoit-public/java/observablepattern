package fr.benoit;

public class Main {

	public static void main(String[] args) {

		WeatherStation station = new WeatherStation();

		PhoneDisplay monPhone = new PhoneDisplay(station);
		PhoneDisplay monPhone1 = new PhoneDisplay(station);
		PhoneDisplay monPhone2 = new PhoneDisplay(station);

		station.add(monPhone);
		station.add(monPhone1);
		station.add(monPhone2);

		station.myNotify();

	}

}

package fr.benoit;

public interface IObserver {

	void update();

}

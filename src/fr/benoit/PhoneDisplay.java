package fr.benoit;

public class PhoneDisplay implements IObserver {

	WeatherStation station;

	public PhoneDisplay(WeatherStation station) {
		super();
		this.station = station;
	}

	public void update() {
		this.station.getTemperature();
	}

	public WeatherStation getStation() {
		return station;
	}

	public void setStation(WeatherStation station) {
		this.station = station;
	}

}

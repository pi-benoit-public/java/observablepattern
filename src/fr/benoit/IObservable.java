package fr.benoit;

public interface IObservable {

	void add(IObserver obs);

	void remove(IObserver obs);

	void myNotify();

}

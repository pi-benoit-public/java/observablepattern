package fr.benoit;

import java.util.List;

public class WeatherStation implements IObservable {

	public List<IObserver> mesObservateurs;
	public String temperature;

	public WeatherStation() {
		super();
	}

	public String getTemperature() {
		return this.temperature;
	}

	@Override
	public void add(IObserver monObservateur) {
		if (!this.mesObservateurs.contains(monObservateur)) {
			this.mesObservateurs.add(monObservateur);
		} else {
			System.err.println("Cet observateur vous observe d�j�.");
		}
	}

	@Override
	public void remove(IObserver monObservateur) {
		int index = this.mesObservateurs.indexOf(monObservateur);
		if (index > 1) {
			System.err.println("L'observateur "+ monObservateur +" n'observe pas.");
			this.mesObservateurs.remove(index);
		}
	}

	@Override
	public void myNotify() {
		for (IObserver obs : mesObservateurs) {
			obs.update();
		}
	}

}
